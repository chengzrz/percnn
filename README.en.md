# PeRCNN
Physics-embedded recurrent convolutional neural network

Codes are available at: [https://gitee.com/mindspore/mindscience/tree/master/MindFlow/applications/data_mechanism_fusion/percnn](https://gitee.com/mindspore/mindscience/tree/master/MindFlow/applications/data_mechanism_fusion/percnn)

Paper link: [[ArXiv](https://arxiv.org/pdf/2106.04781.pdf)] (We will update the final version later...)

By [Chengping Rao](https://scholar.google.com/citations?user=29DpfrEAAAAJ&hl=en), [Pu Ren](https://scholar.google.com/citations?user=7FxlSHEAAAAJ&hl=en), [Yang Liu](https://coe.northeastern.edu/people/liu-yang/), [Hao Sun](https://gsai.ruc.edu.cn/addons/teacher/index/info.html?user_id=0&ruccode=20210163&ln=en)


## Highlights
- Propose a physics-embedded recurrent-convolutional neural network (PeRCNN), which forcibly embeds the physics structure to facilitate learning for data-driven modeling of nonlinear systems
- The physics-embedding mechanism guarantees the model to rigorously obey the given physics based on our prior knowledge
- Present the recurrent π-Block to achieve nonlinear approximation via element-wise product among the feature maps
- Design the spatial information learned by either convolutional or predefined finite-differencebased filters
- Model the temporal evolution with forward Euler time marching scheme
